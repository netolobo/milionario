package com.n3t0l0b0.blogspot.milionario;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {
	
	public TextView dispesasDiversas, diversao, despesasFuturas, instrucaoFinanceira, milionaria, gastoMaximo;
	public EditText montanteRecebido;
	double montante;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		dispesasDiversas = (TextView) findViewById(R.id.despesasDiversas);
		diversao = (TextView) findViewById(R.id.diversao);
		despesasFuturas = (TextView) findViewById(R.id.despesasFuturas);
		instrucaoFinanceira = (TextView) findViewById(R.id.investimentos);
		milionaria = (TextView) findViewById(R.id.contaMilionaria);
		gastoMaximo = (TextView) findViewById(R.id.gastoMaximo);
		
		montanteRecebido = (EditText) findViewById(R.id.montanteRecebido);
		
		
		
		 
		
		
		
		
	}
	
	public void Distribuir(View v) {  
		 
		montante = Double.parseDouble(montanteRecebido.getText().toString());
		
		dispesasDiversas.setText(String.valueOf(calculaPorcentagem(montante, 50)));
		
		diversao.setText(String.valueOf(calculaPorcentagem(montante, 10)));
		
		despesasFuturas.setText(String.valueOf(calculaPorcentagem(montante, 10)));
		
		instrucaoFinanceira.setText(String.valueOf(calculaPorcentagem(montante, 5)));
		
		milionaria.setText(String.valueOf(calculaPorcentagem(montante, 25)));
		
		gastoMaximo.setText(String.valueOf(calculaGastoMáximo(
				calculaPorcentagem(montante, 50),
				calculaPorcentagem(montante, 10),
				calculaPorcentagem(montante, 5)
	
				)));
	     
	}    

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	public double calculaPorcentagem(double montanteRecebido, double porcentagem){
		
		porcentagem = porcentagem/100;
		
		return montanteRecebido*porcentagem;
	}
	
	public double calculaGastoMáximo(double dispesasDiversas, double diversao, double instrucaoFinanceira){
		
		return dispesasDiversas+diversao+instrucaoFinanceira;
	}

}
